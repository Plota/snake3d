
#define MAX_H 15
#define MAX_W 30
#define START 2
#define SCALE 0.3
#define MOVE 2

#include<iostream>
#include "Snake.h"
#include<vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stdlib.h>
#include <stdio.h>
#include "constants.h"
#include "allmodels.h"
#include "lodepng.h"
#include "shaderprogram.h"

using namespace std;


Snake::Snake()
{
    direction = 0;
	length = START+1;

	head_pos_x = 0;
	head_pos_y = 0;
	head_M = glm::mat4(1.0f);
    head_M = glm::scale(head_M, glm::vec3(SCALE,SCALE,SCALE));



    for(int i=0;i<START;i++)
    {
    body_pos_x.push_back(0);
	body_pos_y.push_back(-2*(i+1));
	body_direction.push_back(0);
	rotate_info.push_back(0);
    body_M.push_back(glm::mat4(1.0f));
    body_M[i] = glm::scale(body_M[i], glm::vec3(SCALE,SCALE,SCALE));


    }
}


void Snake::turn_left()
{
	direction = (direction + 1) % 4;
}

void Snake::turn_right()
{
	if (direction == 0) direction = 3;
	else direction -= 1;

}

void Snake::movement()
{
        vector<float> Btmp_x;
        vector<float> Btmp_y;
        vector<float> Btmp_d;



        for(int i=0;i<length-1;i++)
        {
            Btmp_x.push_back(body_pos_x[i]);
            Btmp_y.push_back(body_pos_y[i]);
            Btmp_d.push_back(body_direction[i]);

        }
        if( body_direction[0] == 0 && direction == 1) rotate_info[0] = 2;
        else if( body_direction[0] == 0 && direction == 3) rotate_info[0] = 1;
        else if( body_direction[0] == 1 && direction == 2) rotate_info[0] = 2;
        else if( body_direction[0] == 1 && direction == 0) rotate_info[0] = 1;
        else if( body_direction[0] == 2 && direction == 3) rotate_info[0] = 2;
        else if( body_direction[0] == 2 && direction == 1) rotate_info[0] = 1;
        else if( body_direction[0] == 3 && direction == 0) rotate_info[0] = 2;
        else if( body_direction[0] == 3 && direction == 2) rotate_info[0] = 1;
        else rotate_info[0] = 0;

        body_pos_x[0] = head_pos_x;
        body_pos_y[0] = head_pos_y;
        body_direction[0] = direction;

        for(int i=1;i<length-1;i++)
        {
            body_pos_x[i] = Btmp_x[i-1];
            body_pos_y[i] = Btmp_y[i-1];


            if( body_direction[i] == 0 && Btmp_d[i-1] == 1) rotate_info[i] = 2;
            else if( body_direction[i] == 0 && Btmp_d[i-1] == 3) rotate_info[i] = 1;
            else if( body_direction[i] == 1 && Btmp_d[i-1] == 2) rotate_info[i] = 2;
            else if( body_direction[i] == 1 && Btmp_d[i-1] == 0) rotate_info[i] = 1;
            else if( body_direction[i] == 2 && Btmp_d[i-1] == 3) rotate_info[i] = 2;
            else if( body_direction[i] == 2 && Btmp_d[i-1] == 1) rotate_info[i] = 1;
            else if( body_direction[i] == 3 && Btmp_d[i-1] == 0) rotate_info[i] = 2;
            else if( body_direction[i] == 3 && Btmp_d[i-1] == 2) rotate_info[i] = 1;
            else rotate_info[i] = 0;

            body_direction[i] = Btmp_d[i-1];

        }

    switch(direction)
    {
    case 0:
        head_pos_y += MOVE;
        break;

    case 1:
        head_pos_x += MOVE;
        break;

    case 2:
        head_pos_y -= MOVE;
        break;

    case 3:
        head_pos_x -= MOVE;
        break;

    }


}

glm::mat4 Snake::designate_head_position()
{
    return glm::translate(head_M, glm::vec3(head_pos_x,head_pos_y,0.0f));
}

glm::mat4 Snake::designate_position(int i)
{
    return glm::translate(body_M[i], glm::vec3(body_pos_x[i],body_pos_y[i],0.0f));
}


void Snake::eat()
{
    length++;
    body_M.push_back(glm::mat4(1.0f));
    body_M.back()= glm::scale(body_M.back(), glm::vec3(SCALE,SCALE,SCALE));
    body_pos_x.push_back(-100); //za map�
    body_pos_y.push_back(-100); //za map�
    body_direction.push_back(0);
    rotate_info.push_back(0);



}


int Snake::Get_rotate_info(int i)
{
    return rotate_info[i];
}

float Snake::Get_body_pos_x(int i)
{
    return body_pos_x[i];
}

float Snake::Get_body_pos_y(int i)
{
    return body_pos_y[i];
}

float Snake::Get_head_pos_x()
{
    return head_pos_x;
}

float Snake::Get_head_pos_y()
{
    return head_pos_y;
}

int Snake::Get_head_direction()
{
    return direction;
}

int Snake::Get_body_direction(int i)
{
    return body_direction[i];
}

bool Snake::has_colision()
{
    if(head_pos_x<(-MAX_W) || head_pos_x > MAX_W || head_pos_y<(-MAX_H-1) || head_pos_y > MAX_H+1)
        return true;
    else
        {
            for(int i=0;i<length-1;i++)
                {
                    float x = body_pos_x[i];
                    float y = body_pos_y[i];
                    float a = head_pos_x;
                    float b = head_pos_y;
                    if( x == a && y == b )
                        return true;
                }
            return false;
        }
}


Snake::~Snake()
{
    //dtor
}

#ifndef SNAKE_H
#define SNAKE_H
#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stdlib.h>
#include <stdio.h>
#include "constants.h"
#include "allmodels.h"
#include "lodepng.h"
#include "shaderprogram.h"
class Snake
{
    public:
        Snake();
        virtual ~Snake();
		void movement();
		void turn_left();
		void turn_right();
        bool has_colision();
		void eat();
        float Get_head_pos_x();
        float Get_head_pos_y();
        float Get_body_pos_x(int parameter);
        float Get_body_pos_y(int parameter);
        int Get_head_direction();
        int Get_body_direction(int parameter);
        int Get_rotate_info(int parameter);
		glm::mat4 designate_head_position();
		glm::mat4 designate_position(int parameter);
		int length;
    protected:

    private:
		int direction;
		float head_pos_x;
		float head_pos_y;
		std::vector<float> body_pos_x;
		std::vector<float> body_pos_y;
		std::vector<int> body_direction;
		std::vector<int> rotate_info; //do zgi��
		glm::mat4 head_M;
		std::vector <glm::mat4> body_M;


};

#endif // SNAKE_H

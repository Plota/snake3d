#include "fence.h"
namespace Models {
    Fence::Fence()
    {
        ObjectLoader::loadOBJ("models/plot/plot.obj", vertices, uvs, normals, colors);
    }

    void Fence::loadTexture() {
        texture = ObjectLoader::loadTexture("models/plot/p.png");
    };

    std::vector< glm::vec4 > Fence::get_vertices() {
        return vertices;
    };

    std::vector< glm::vec2 > Fence::get_uvs() {
        return uvs;
    };

    std::vector< glm::vec4 > Fence::get_normals() {
        return normals;
    };

    std::vector< glm::vec4 > Fence::get_colors() {
        return colors;
    };

    GLuint Fence::get_texture() {
        return texture;
    }

    int Fence::get_vericles_count() {
        return vertices.size();
    }

    Fence::~Fence()
    {
        //dtor
    }
}

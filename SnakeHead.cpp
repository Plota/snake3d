#include "SnakeHead.h"

namespace Models {
    SnakeHead::SnakeHead()
    {
        ObjectLoader::loadOBJ("models/wonsz/glowa_wonsz.obj", vertices, uvs, normals, colors);
    }

    void SnakeHead::loadTexture() {
        texture = ObjectLoader::loadTexture("models/wonsz/glowa_wonsz.png");
    };

    std::vector< glm::vec4 > SnakeHead::get_vertices() {
        return vertices;
    };

    std::vector< glm::vec2 > SnakeHead::get_uvs() {
        return uvs;
    };

    std::vector< glm::vec4 > SnakeHead::get_normals() {
        return normals;
    };

    std::vector< glm::vec4 > SnakeHead::get_colors() {
        return colors;
    };

    int SnakeHead::get_vericles_count() {
        return vertices.size();
    }

    GLuint SnakeHead::get_texture() {
        return texture;
    }

    SnakeHead::~SnakeHead()
    {
        //dtor
    }
}

#include "SnakeTail.h"

namespace Models {
    SnakeTail::SnakeTail()
    {
        ObjectLoader::loadOBJ("models/wonsz/ogon_wonsz.obj", vertices, uvs, normals, colors);
    }

    std::vector< glm::vec4 > SnakeTail::get_vertices() {
        return vertices;
    };

    std::vector< glm::vec2 > SnakeTail::get_uvs() {
        return uvs;
    };

    std::vector< glm::vec4 > SnakeTail::get_normals() {
        return normals;
    };

    std::vector< glm::vec4 > SnakeTail::get_colors() {
        return colors;
    };

    int SnakeTail::get_vericles_count() {
        return vertices.size();
    }

    SnakeTail::~SnakeTail()
    {
        //dtor
    }
}

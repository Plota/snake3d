#ifndef APPLE_H
#define APPLE_H
#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stdlib.h>
#include <stdio.h>
#include "constants.h"
#include "allmodels.h"
#include "lodepng.h"
#include "shaderprogram.h"
#include "ObjectLoader.h"
#include <cstdlib>
#include <ctime>

class Apple
{
    public:
        Apple();
        glm::mat4 designate_apple_position();
        void is_eaten();
        virtual ~Apple();
        float get_x();
        float get_y();
        void loadTexture();
        std::vector< glm::vec4 > get_vertices();
        std::vector< glm::vec2 > get_uvs();
        std::vector< glm::vec4 > get_normals();
        std::vector< glm::vec4 > get_colors();
        GLuint get_texture();
        int get_vericles_count();
        void drawSolid();
    protected:

    private:
        std::vector< glm::vec4 > vertices;
        std::vector< glm::vec2 > uvs;
        std::vector< glm::vec4 > normals;
        std::vector< glm::vec4 > colors;
        float pos_x;
        float pos_y;
        glm::mat4 apple_M;
        GLuint texture;
};

#endif // APPLE_H

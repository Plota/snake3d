#include "Grass.h"

namespace Models {
    Grass::Grass()
    {
        ObjectLoader::loadOBJ("models/grass/grass.obj", vertices, uvs, normals, colors);
    }

    void Grass::loadTexture() {
        texture = ObjectLoader::loadTexture("models/grass/g.png");
    };

    std::vector< glm::vec4 > Grass::get_vertices() {
        return vertices;
    };

    std::vector< glm::vec2 > Grass::get_uvs() {
        return uvs;
    };

    std::vector< glm::vec4 > Grass::get_normals() {
        return normals;
    };

    std::vector< glm::vec4 > Grass::get_colors() {
        return colors;
    };

    glm::mat4 Grass::get_M_matrix() {
        glm::mat4 M = glm::mat4(1.0f);
        M = glm::rotate(M, 90.0f*PI/180.0f, glm::vec3(1.0f,0.0f,0.0f));
        M = glm::rotate(M, 90.0f*PI/180.0f, glm::vec3(0.0f,1.0f,0.0f));
        M = glm::scale(M, glm::vec3(2, 2, 2));
        M = glm::translate(M, glm::vec3(0,0.4,0));
        return M;
    }

    GLuint Grass::get_texture() {
        return texture;
    }

    int Grass::get_vericles_count() {
        return vertices.size();
    }

    Grass::~Grass()
    {
        //dtor
    }
}

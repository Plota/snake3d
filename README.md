# SNAKE 3D #
Projekt został wykonany w technologii OpenGL. Modele stworzono w Blenderze.

Gra polega na kierowaniu ruchem "węża" za pomocą strzałek tak aby zjadać jak najwięcej jabłek. Model porusza sie samoczynnie. Zjedzenie Jabłka skutkuje wydłużeniem sie ciała węża o jeden kwadrat. Próba zjedenia własnego ogona lub wjechania w ściane powoduje zakończenie rozgrywki.

Screenshoty z gry:

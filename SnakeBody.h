#ifndef SNAKEBODY_H
#define SNAKEBODY_H

#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include "ObjectLoader.h"

namespace Models {
    class SnakeBody
    {
        public:
            SnakeBody();
            virtual ~SnakeBody();
            void loadTexture();
            std::vector< glm::vec4 > get_vertices();
            std::vector< glm::vec2 > get_uvs();
            std::vector< glm::vec4 > get_normals();
            std::vector< glm::vec4 > get_colors();
            GLuint get_texture();
            int get_vericles_count();

        private:
            std::vector< glm::vec4 > vertices;
            std::vector< glm::vec2 > uvs;
            std::vector< glm::vec4 > normals;
            std::vector< glm::vec4 > colors;
            GLuint texture;
    };
}
#endif // SNAKEBODY_H

#ifndef SNAKETAIL_H
#define SNAKETAIL_H

#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include "ObjectLoader.h"

namespace Models {
    class SnakeTail
    {
        public:
            SnakeTail();
            virtual ~SnakeTail();
            std::vector< glm::vec4 > get_vertices();
            std::vector< glm::vec2 > get_uvs();
            std::vector< glm::vec4 > get_normals();
            std::vector< glm::vec4 > get_colors();
            int get_vericles_count();

        private:
            std::vector< glm::vec4 > vertices;
            std::vector< glm::vec2 > uvs;
            std::vector< glm::vec4 > normals;
            std::vector< glm::vec4 > colors;
    };
}

#endif // SNAKETAIL_H

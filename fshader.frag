#version 330

uniform sampler2D textureMap0; //globalnie

out vec4 pixelColor; //Zmienna wyjsciowa fragment shadera. Zapisuje sie do niej ostateczny (prawie) kolor piksela

in vec2 iTexCoord0; //globalnie

void main(void) {
	vec4 texColor=texture(textureMap0,iTexCoord0);//main
	pixelColor = texColor;
}

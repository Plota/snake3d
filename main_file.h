#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include "constants.h"
#include "allmodels.h"
#include "lodepng.h"
#include "shaderprogram.h"
#include "Snake.h"
#include "Apple.h"
#include "fence.h"
#include "SnakeBody.h"
#include "SnakeBodyCorner.h"
#include <windows.h>
#include <mmsystem.h>

// obiekty
Snake wonsz;
Apple jabzo;
Models::Fence plot;
Models::SnakeHead wonszGlowa;
Models::SnakeTail wonszOgon;
Models::SnakeBody wonszCialo;
Models::SnakeBodyCorner wonszCialoRog;
Models::Grass grass;

//Uchwyty na VAO i bufory wierzcho³ków
GLuint jabzoVao;
GLuint cubeVao;
GLuint plotVao;
GLuint wonszGlowaVao;
GLuint wonszOgonVao;
GLuint wonszCialoVao;
GLuint wonszCialoRogVao;
GLuint grassVao;

GLuint bufVertices; //Uchwyt na bufor VBO przechowuj¹cy tablicê wspó³rzêdnych wierzcho³ków
GLuint bufColors;  //Uchwyt na bufor VBO przechowuj¹cy tablicê kolorów
GLuint bufNormals; //Uchwyt na bufor VBO przechowuj¹cy tablickê wektorów normalnych
GLuint bufTexCoords;

float* vertices=Models::CubeInternal::vertices;
float* colors=Models::CubeInternal::colors;
float* normals=Models::CubeInternal::normals;
int vertexCount=Models::CubeInternal::vertexCount;

void ramka(glm::mat4 P, glm::mat4 V);

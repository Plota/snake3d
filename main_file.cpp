/*
Niniejszy program jest wolnym oprogramowaniem; mo¿esz go
rozprowadzaæ dalej i / lub modyfikowaæ na warunkach Powszechnej
Licencji Publicznej GNU, wydanej przez Fundacjê Wolnego
Oprogramowania - wed³ug wersji 2 tej Licencji lub(wed³ug twojego
wyboru) którejœ z póŸniejszych wersji.

Niniejszy program rozpowszechniany jest z nadziej¹, i¿ bêdzie on
u¿yteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyœlnej
gwarancji PRZYDATNOŒCI HANDLOWEJ albo PRZYDATNOŒCI DO OKREŒLONYCH
ZASTOSOWAÑ.W celu uzyskania bli¿szych informacji siêgnij do
Powszechnej Licencji Publicznej GNU.

Z pewnoœci¹ wraz z niniejszym programem otrzyma³eœ te¿ egzemplarz
Powszechnej Licencji Publicznej GNU(GNU General Public License);
jeœli nie - napisz do Free Software Foundation, Inc., 59 Temple
Place, Fifth Floor, Boston, MA  02110 - 1301  USA
*/

#define MAX_H 14
#define MAX_W 28
#define SCALE 0.3


#define GLM_FORCE_RADIANS

#include "main_file.h"

using namespace glm;

int ruch = 0;
int pause = 0;
bool music = 0;

int konami = 0;
int kamera = 1;

float aspect=(float)16/9; //Stosunek szerokoœci do wysokoœci okna

//Uchwyty na shadery
ShaderProgram *shaderProgram; //WskaŸnik na obiekt reprezentuj¹cy program cieniuj¹cy.

//Procedura obs³ugi b³êdów
void error_callback(int error, const char* description) {
	fputs(description, stderr);
}

//Procedura obs³ugi klawiatury
void key_callback(GLFWwindow* window, int key,
	int scancode, int action, int mods) {
	if (action == GLFW_PRESS) {
        if (key == GLFW_KEY_UP)
        {

            if(konami == 0)konami++;
            else if(konami == 1)konami++;
            else konami = 0;
        }
        if (key == GLFW_KEY_DOWN)
        {

            if(konami == 2)konami++;
            else if(konami == 3)konami++;
            else konami = 0;
        }
		if (key == GLFW_KEY_LEFT)
        {
            wonsz.turn_left();
            if(konami == 4)konami++;
            else if(konami == 6)konami++;
            else konami = 0;
        }
		if (key == GLFW_KEY_RIGHT)
        {
            wonsz.turn_right();
            if(konami == 5)konami++;
            else if(konami == 7)konami++;
            else konami = 0;
        }
        if (key == GLFW_KEY_A)
        {
            if(konami == 9)
            {
                PlaySound("MNH.wav", NULL, SND_FILENAME | SND_ASYNC);
                music = true;
            }
            else konami = 0;
        }
        if (key == GLFW_KEY_B)
        {
            if(konami == 8)
            {
            konami++;
            }
            else konami = 0;
        }
		if (key == GLFW_KEY_SPACE)
        {
            wonsz.eat();
        }
        if (key == GLFW_KEY_E)
        {
            jabzo.is_eaten();
        }
        if (key == GLFW_KEY_P)
        {
            if(pause==0) pause = 1;
            else if(pause==1) pause = 0;
        }
        if (key == GLFW_KEY_ESCAPE)
        {
            glfwDestroyWindow(window); //Usuñ kontekst OpenGL i okno
        }
        if (key == GLFW_KEY_F1)
        {
            kamera = 1;
        }
        if (key == GLFW_KEY_F2)
        {
            kamera = 2;
        }
          if (key == GLFW_KEY_M)
        {
            PlaySound("MNH.wav", NULL, SND_FILENAME | SND_ASYNC);
            music = true;

        }


	}


	if (action == GLFW_RELEASE) {

	}
}

//Procedura ob³ugi zmiany rozmiaru bufora ramki
void windowResize(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height); //Obraz ma byæ generowany w oknie o tej rozdzielczoœci
    if (height!=0) {
        aspect=(float)width/(float)height; //Stosunek szerokoœci do wysokoœci okna
    } else {
        aspect=(float)16/9;
    }
}

GLuint readTexture(char* filename) {
  GLuint tex;
  glActiveTexture(GL_TEXTURE0);

  //Wczytanie do pamięci komputera
  std::vector<unsigned char> image;   //Alokuj wektor do wczytania obrazka
  unsigned width, height;   //Zmienne do których wczytamy wymiary obrazka
  //Wczytaj obrazek
  unsigned error = lodepng::decode(image, width, height, filename);

  //Import do pamięci karty graficznej
  glGenTextures(1,&tex); //Zainicjuj jeden uchwyt
  glBindTexture(GL_TEXTURE_2D, tex); //Uaktywnij uchwyt
  //Wczytaj obrazek do pamięci KG skojarzonej z uchwytem
  glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0,
    GL_RGBA, GL_UNSIGNED_BYTE, (unsigned char*) image.data());

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);

  return tex;
}
//Tworzy bufor VBO z tablicy
GLuint makeBuffer(void *data, int vertexCount, int vertexSize) {
	GLuint handle;

	glGenBuffers(1,&handle);//Wygeneruj uchwyt na Vertex Buffer Object (VBO), który bêdzie zawiera³ tablicê danych
	glBindBuffer(GL_ARRAY_BUFFER,handle);  //Uaktywnij wygenerowany uchwyt VBO
	glBufferData(GL_ARRAY_BUFFER, vertexCount*vertexSize, data, GL_STATIC_DRAW);//Wgraj tablicê do VBO

	return handle;
}

//Przypisuje bufor VBO do atrybutu
void assignVBOtoAttribute(ShaderProgram *shaderProgram,const char* attributeName, GLuint bufVBO, int vertexSize) {
	GLuint location=shaderProgram->getAttribLocation(attributeName); //Pobierz numer slotu dla atrybutu
	glBindBuffer(GL_ARRAY_BUFFER,bufVBO);  //Uaktywnij uchwyt VBO
	glEnableVertexAttribArray(location); //W³¹cz u¿ywanie atrybutu o numerze slotu zapisanym w zmiennej location
	glVertexAttribPointer(location,vertexSize,GL_FLOAT, GL_FALSE, 0, NULL); //Dane do slotu location maj¹ byæ brane z aktywnego VBO
}


void prepareObjectCube(ShaderProgram *shaderProgram) {
	//Zbuduj VBO z danymi obiektu do narysowania
	bufVertices=makeBuffer(vertices, vertexCount, sizeof(float)*4); //VBO ze współrzędnymi wierzchołków
	bufColors=makeBuffer(colors, vertexCount, sizeof(float)*4);//VBO z kolorami wierzchołków
	bufNormals=makeBuffer(normals, vertexCount, sizeof(float)*4);//VBO z wektorami normalnymi wierzchołków


	//Zbuduj VAO wiążący atrybuty z konkretnymi VBO
	glGenVertexArrays(1,&cubeVao); //Wygeneruj uchwyt na VAO i zapisz go do zmiennej globalnej

	glBindVertexArray(cubeVao); //Uaktywnij nowo utworzony VAO

	assignVBOtoAttribute(shaderProgram,"vertex",bufVertices,4); //"vertex" odnosi się do deklaracji "in vec4 vertex;" w vertex shaderze
	assignVBOtoAttribute(shaderProgram,"color",bufColors,4); //"color" odnosi się do deklaracji "in vec4 color;" w vertex shaderze
	assignVBOtoAttribute(shaderProgram,"normal",bufNormals,4); //"normal" odnosi się do deklaracji "in vec4 normal;" w vertex shaderze

	glBindVertexArray(0); //Dezaktywuj VAO
}
//Przygotowanie do rysowania pojedynczego obiektu
void prepareObjectApple(ShaderProgram *shaderProgram) {

	//Zbuduj VBO z danymi obiektu do narysowania
	bufVertices=makeBuffer(jabzo.get_vertices().data(), jabzo.get_vericles_count(), sizeof(float)*4); //VBO ze wspó³rzêdnymi wierzcho³ków
	bufNormals=makeBuffer(jabzo.get_normals().data(), jabzo.get_vericles_count(), sizeof(float)*4);//VBO z wektorami normalnymi wierzcho³ków
	bufTexCoords=makeBuffer(jabzo.get_uvs().data(),jabzo.get_vericles_count(),sizeof(float)*2);

    jabzo.loadTexture();
	//Zbuduj VAO wi¹¿¹cy atrybuty z konkretnymi VBO
	glGenVertexArrays(1,&jabzoVao); //Wygeneruj uchwyt na VAO i zapisz go do zmiennej globalnej

	glBindVertexArray(jabzoVao); //Uaktywnij nowo utworzony VAO

	assignVBOtoAttribute(shaderProgram,"vertex",bufVertices,4); //"vertex" odnosi siê do deklaracji "in vec4 vertex;" w vertex shaderze
	assignVBOtoAttribute(shaderProgram,"normal",bufNormals,4); //"normal" odnosi siê do deklaracji "in vec4 normal;" w vertex shaderze
    assignVBOtoAttribute(shaderProgram,"texCoord0", bufTexCoords,2);

	glBindVertexArray(0); //Dezaktywuj VAO
}

void prepareObjectFence(ShaderProgram *shaderProgram) {

	//Zbuduj VBO z danymi obiektu do narysowania
	bufVertices=makeBuffer(plot.get_vertices().data(), plot.get_vericles_count(), sizeof(float)*4); //VBO ze wspó³rzêdnymi wierzcho³ków
	bufNormals=makeBuffer(plot.get_normals().data(), plot.get_vericles_count(), sizeof(float)*4);//VBO z wektorami normalnymi wierzcho³ków
	bufTexCoords=makeBuffer(plot.get_uvs().data(),plot.get_vericles_count(),sizeof(float)*2);

    plot.loadTexture();

	//Zbuduj VAO wi¹¿¹cy atrybuty z konkretnymi VBO
	glGenVertexArrays(1,&plotVao); //Wygeneruj uchwyt na VAO i zapisz go do zmiennej globalnej

	glBindVertexArray(plotVao); //Uaktywnij nowo utworzony VAO

	assignVBOtoAttribute(shaderProgram,"vertex",bufVertices,4); //"vertex" odnosi siê do deklaracji "in vec4 vertex;" w vertex shaderze
	assignVBOtoAttribute(shaderProgram,"normal",bufNormals,4); //"normal" odnosi siê do deklaracji "in vec4 normal;" w vertex shaderze
	assignVBOtoAttribute(shaderProgram,"texCoord0", bufTexCoords,2);

	glBindVertexArray(0); //Dezaktywuj VAO
}

void prepareObjectWonszGlowa(ShaderProgram *shaderProgram) {

	//Zbuduj VBO z danymi obiektu do narysowania
	bufVertices=makeBuffer(wonszGlowa.get_vertices().data(), wonszGlowa.get_vericles_count(), sizeof(float)*4); //VBO ze wspó³rzêdnymi wierzcho³ków
	bufNormals=makeBuffer(wonszGlowa.get_normals().data(), wonszGlowa.get_vericles_count(), sizeof(float)*4);//VBO z wektorami normalnymi wierzcho³ków
    bufTexCoords=makeBuffer(wonszGlowa.get_uvs().data(),wonszGlowa.get_vericles_count(),sizeof(float)*2);

    wonszGlowa.loadTexture();
	//Zbuduj VAO wi¹¿¹cy atrybuty z konkretnymi VBO
	glGenVertexArrays(1,&wonszGlowaVao); //Wygeneruj uchwyt na VAO i zapisz go do zmiennej globalnej

	glBindVertexArray(wonszGlowaVao); //Uaktywnij nowo utworzony VAO

	assignVBOtoAttribute(shaderProgram,"vertex",bufVertices,4); //"vertex" odnosi siê do deklaracji "in vec4 vertex;" w vertex shaderze
	assignVBOtoAttribute(shaderProgram,"normal",bufNormals,4); //"normal" odnosi siê do deklaracji "in vec4 normal;" w vertex shaderze
    assignVBOtoAttribute(shaderProgram,"texCoord0", bufTexCoords,2);

	glBindVertexArray(0); //Dezaktywuj VAO
}

void prepareObjectWonszOgon(ShaderProgram *shaderProgram) {

	//Zbuduj VBO z danymi obiektu do narysowania
	bufVertices=makeBuffer(wonszOgon.get_vertices().data(), wonszOgon.get_vericles_count(), sizeof(float)*4); //VBO ze wspó³rzêdnymi wierzcho³ków
	bufNormals=makeBuffer(wonszOgon.get_normals().data(), wonszOgon.get_vericles_count(), sizeof(float)*4);//VBO z wektorami normalnymi wierzcho³ków
    bufTexCoords=makeBuffer(wonszOgon.get_uvs().data(),wonszOgon.get_vericles_count(),sizeof(float)*2);

	//Zbuduj VAO wi¹¿¹cy atrybuty z konkretnymi VBO
	glGenVertexArrays(1,&wonszOgonVao); //Wygeneruj uchwyt na VAO i zapisz go do zmiennej globalnej

	glBindVertexArray(wonszOgonVao); //Uaktywnij nowo utworzony VAO

	assignVBOtoAttribute(shaderProgram,"vertex",bufVertices,4); //"vertex" odnosi siê do deklaracji "in vec4 vertex;" w vertex shaderze
	assignVBOtoAttribute(shaderProgram,"normal",bufNormals,4); //"normal" odnosi siê do deklaracji "in vec4 normal;" w vertex shaderze
    assignVBOtoAttribute(shaderProgram,"texCoord0", bufTexCoords,2);

	glBindVertexArray(0); //Dezaktywuj VAO
}


void prepareObjectWonszBody(ShaderProgram *shaderProgram) {

	//Zbuduj VBO z danymi obiektu do narysowania
	bufVertices=makeBuffer(wonszCialo.get_vertices().data(), wonszCialo.get_vericles_count(), sizeof(float)*4); //VBO ze wspó³rzêdnymi wierzcho³ków
	bufNormals=makeBuffer(wonszCialo.get_normals().data(), wonszCialo.get_vericles_count(), sizeof(float)*4);//VBO z wektorami normalnymi wierzcho³ków
    bufTexCoords=makeBuffer(wonszCialo.get_uvs().data(),wonszCialo.get_vericles_count(),sizeof(float)*2);

    wonszCialo.loadTexture();
	//Zbuduj VAO wi¹¿¹cy atrybuty z konkretnymi VBO
	glGenVertexArrays(1,&wonszCialoVao); //Wygeneruj uchwyt na VAO i zapisz go do zmiennej globalnej

	glBindVertexArray(wonszCialoVao); //Uaktywnij nowo utworzony VAO

	assignVBOtoAttribute(shaderProgram,"vertex",bufVertices,4); //"vertex" odnosi siê do deklaracji "in vec4 vertex;" w vertex shaderze
	assignVBOtoAttribute(shaderProgram,"normal",bufNormals,4); //"normal" odnosi siê do deklaracji "in vec4 normal;" w vertex shaderze
    assignVBOtoAttribute(shaderProgram,"texCoord0", bufTexCoords,2);

	glBindVertexArray(0); //Dezaktywuj VAO
}

void prepareObjectWonszBodyCorner(ShaderProgram *shaderProgram) {

	//Zbuduj VBO z danymi obiektu do narysowania
	bufVertices=makeBuffer(wonszCialoRog.get_vertices().data(), wonszCialoRog.get_vericles_count(), sizeof(float)*4); //VBO ze wspó³rzêdnymi wierzcho³ków
	bufNormals=makeBuffer(wonszCialoRog.get_normals().data(), wonszCialoRog.get_vericles_count(), sizeof(float)*4);//VBO z wektorami normalnymi wierzcho³ków
    bufTexCoords=makeBuffer(wonszCialoRog.get_uvs().data(),wonszCialoRog.get_vericles_count(),sizeof(float)*2);

	//Zbuduj VAO wi¹¿¹cy atrybuty z konkretnymi VBO
	glGenVertexArrays(1,&wonszCialoRogVao); //Wygeneruj uchwyt na VAO i zapisz go do zmiennej globalnej

	glBindVertexArray(wonszCialoRogVao); //Uaktywnij nowo utworzony VAO

	assignVBOtoAttribute(shaderProgram,"vertex",bufVertices,4); //"vertex" odnosi siê do deklaracji "in vec4 vertex;" w vertex shaderze
	assignVBOtoAttribute(shaderProgram,"normal",bufNormals,4); //"normal" odnosi siê do deklaracji "in vec4 normal;" w vertex shaderze
    assignVBOtoAttribute(shaderProgram,"texCoord0", bufTexCoords,2);

	glBindVertexArray(0); //Dezaktywuj VAO
}

void prepareObjectGrass(ShaderProgram *shaderProgram) {

	//Zbuduj VBO z danymi obiektu do narysowania
	bufVertices=makeBuffer(grass.get_vertices().data(), grass.get_vericles_count(), sizeof(float)*4); //VBO ze wspó³rzêdnymi wierzcho³ków
	bufNormals=makeBuffer(grass.get_normals().data(), grass.get_vericles_count(), sizeof(float)*4);//VBO z wektorami normalnymi wierzcho³ków
    bufTexCoords=makeBuffer(grass.get_uvs().data(),grass.get_vericles_count(),sizeof(float)*2);

    grass.loadTexture();

	//Zbuduj VAO wi¹¿¹cy atrybuty z konkretnymi VBO
	glGenVertexArrays(1,&grassVao); //Wygeneruj uchwyt na VAO i zapisz go do zmiennej globalnej

	glBindVertexArray(grassVao); //Uaktywnij nowo utworzony VAO

	assignVBOtoAttribute(shaderProgram,"vertex",bufVertices,4); //"vertex" odnosi siê do deklaracji "in vec4 vertex;" w vertex shaderze
	assignVBOtoAttribute(shaderProgram,"normal",bufNormals,4); //"normal" odnosi siê do deklaracji "in vec4 normal;" w vertex shaderze
    assignVBOtoAttribute(shaderProgram,"texCoord0", bufTexCoords,2);

	glBindVertexArray(0); //Dezaktywuj VAO
}

//Procedura inicjuj¹ca
void initOpenGLProgram(GLFWwindow* window) {
	//************Tutaj umieszczaj kod, kt�ry nale�y wykona� raz, na pocz�tku programu************
	glClearColor(0, 0, 0, 1); //Czy�� ekran na czarno
	glEnable(GL_DEPTH_TEST); //W��cz u�ywanie Z-Bufora
	glfwSetKeyCallback(window, key_callback); //Zarejestruj procedur� obs�ugi klawiatury
    glfwSetFramebufferSizeCallback(window,windowResize); //Zarejestruj procedur� obs�ugi zmiany rozmiaru bufora ramki

	shaderProgram=new ShaderProgram("vs.glsl",NULL,"fs.glsl");

    prepareObjectApple(shaderProgram);
    prepareObjectCube(shaderProgram);
    prepareObjectFence(shaderProgram);
    prepareObjectWonszGlowa(shaderProgram);
    prepareObjectWonszOgon(shaderProgram);
    prepareObjectWonszBody(shaderProgram);
    prepareObjectWonszBodyCorner(shaderProgram);
    prepareObjectGrass(shaderProgram);


}

//Zwolnienie zasobów zajêtych przez program
void freeOpenGLProgram() {
	delete shaderProgram; //Usuniêcie programu cieniuj¹cego

	glDeleteVertexArrays(1,&jabzoVao); //Usuniêcie vao
	glDeleteVertexArrays(1,&cubeVao); //Usuniêcie vao
	glDeleteVertexArrays(1,&plotVao); //Usuniêcie vao
	glDeleteVertexArrays(1,&wonszGlowaVao); //Usuniêcie vao
	glDeleteVertexArrays(1,&wonszOgonVao); //Usuniêcie vao
	glDeleteVertexArrays(1,&wonszCialoVao); //Usuniêcie vao
	glDeleteVertexArrays(1,&wonszCialoRogVao); //Usuniêcie vao
	glDeleteVertexArrays(1,&grassVao); //Usuniêcie vao
	glDeleteBuffers(1,&bufVertices); //Usuniêcie VBO z wierzcho³kami
	glDeleteBuffers(1,&bufColors); //Usuniêcie VBO z kolorami
	glDeleteBuffers(1,&bufNormals); //Usuniêcie VBO z wektorami normalnymi


}

void drawObject(GLuint vao, ShaderProgram *shaderProgram, mat4 mP, mat4 mV, mat4 mM, int verticesCount, GLuint texture) {
	//W³¹czenie programu cieniuj¹cego, który ma zostaæ u¿yty do rysowania
	//W tym programie wystarczy³oby wywo³aæ to raz, w setupShaders, ale chodzi o pokazanie,
	//¿e mozna zmieniaæ program cieniuj¹cy podczas rysowania jednej sceny
	shaderProgram->use();

	//Przeka¿ do shadera macierze P,V i M.
	//W linijkach poni¿ej, polecenie:
	//  shaderProgram->getUniformLocation("P")
	//pobiera numer slotu odpowiadaj¹cego zmiennej jednorodnej o podanej nazwie
	//UWAGA! "P" w powy¿szym poleceniu odpowiada deklaracji "uniform mat4 P;" w vertex shaderze,
	//a mP w glm::value_ptr(mP) odpowiada argumentowi  "mat4 mP;" TYM pliku.
	//Ca³a poni¿sza linijka przekazuje do zmiennej jednorodnej P w vertex shaderze dane z argumentu mP niniejszej funkcji
	//Pozosta³e polecenia dzia³aj¹ podobnie.

	glUniformMatrix4fv(shaderProgram->getUniformLocation("P"),1, false, glm::value_ptr(mP));
	glUniformMatrix4fv(shaderProgram->getUniformLocation("V"),1, false, glm::value_ptr(mV));
	glUniformMatrix4fv(shaderProgram->getUniformLocation("M"),1, false, glm::value_ptr(mM));
    glUniform1i(shaderProgram->getUniformLocation("diffuseMap"),0);
	//Uaktywnienie VAO i tym samym uaktywnienie predefiniowanych w tym VAO powi¹zañ slotów atrybutów z tablicami z danymi

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,texture);
	//Narysowanie obiektu
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES,0,verticesCount);

	//Posprz¹tanie po sobie (niekonieczne w sumie je¿eli korzystamy z VAO dla ka¿dego rysowanego obiektu)
	glBindVertexArray(0);
}

//Procedura rysuj¹ca zawartoœæ sceny
void drawScene(GLFWwindow* window) {
	//************Tutaj umieszczaj kod rysuj¹cy obraz******************l

	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT); //Wykonaj czyszczenie bufora kolorów

	glm::mat4 P = glm::perspective(50 * PI / 180, aspect, 2.0f, 200.0f); //Wylicz macierz rzutowania

    glm::mat4 V;
    switch (kamera)
    {
    case 1:
        {
        V = glm::lookAt( //Wylicz macierz widoku
		glm::vec3(0.0f, -7.0f, -13.0f),
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f));
		break;
        }
    case 2:
        {
        V = glm::lookAt( //Wylicz macierz widoku
		glm::vec3(0.0f, 0.0f, -13.0f),
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f));
		break;
        }
    }
    //drawObject(grassVao, shaderProgram, P, V, grass.get_M_matrix(), grass.get_vericles_count(), grass.get_texture());
for(int j=-4;j<=4;j++)
{
    for(int i=-5;i<=4;i++)
    {
    glm::mat4 tmp = grass.get_M_matrix();
    tmp = glm::translate(tmp, glm::vec3(-1.2*i,0,1.99*j));
    drawObject(grassVao, shaderProgram, P, V, tmp, grass.get_vericles_count(), grass.get_texture());
    }
}

    glm::mat4 M= wonsz.designate_head_position();
    M = glm::rotate(M, -90.0f*PI/180.0f, glm::vec3(1.0f,0.0f,0.0f));
    drawObject(wonszGlowaVao,shaderProgram,P,V,M, wonszGlowa.get_vericles_count(), wonszGlowa.get_texture());

    glm::mat4 Mb[wonsz.length-1];
    for (int i=0;i<wonsz.length-1;i++)
    {
        Mb[i] = wonsz.designate_position(i);
        Mb[i] = glm::rotate(Mb[i], 270.0f*PI/180.0f, glm::vec3(1.0f,0.0f,0.0f));
        switch(wonsz.Get_body_direction(i))
        {
            case 0:
                Mb[i] = glm::rotate(Mb[i], 0.0f*PI/180.0f, glm::vec3(0.0f,1.0f,0.0f));
                break;
            case 1:
                Mb[i] = glm::rotate(Mb[i], 90.0f*PI/180.0f, glm::vec3(0.0f,1.0f,0.0f));
                break;
            case 2:
                Mb[i] = glm::rotate(Mb[i], 180.0f*PI/180.0f, glm::vec3(0.0f,1.0f,0.0f));
                break;
            case 3:
                Mb[i] = glm::rotate(Mb[i], 270.0f*PI/180.0f, glm::vec3(0.0f,1.0f,0.0f));
                break;
        }

        int x = wonsz.Get_rotate_info(i);
        if (i == wonsz.length - 2) {
            Mb[i] = glm::rotate(Mb[i], PI, glm::vec3(0.0f,1.0f,0.0f));
            drawObject(wonszOgonVao,shaderProgram,P,V,Mb[i], wonszCialo.get_vericles_count(), wonszCialo.get_texture());
            continue;
        }
        if(wonsz.Get_rotate_info(i)==0) {

            drawObject(wonszCialoVao,shaderProgram,P,V,Mb[i], wonszCialo.get_vericles_count(), wonszCialo.get_texture());
        } else if(wonsz.Get_rotate_info(i)==1) {
            Mb[i] = glm::rotate(Mb[i], 270.0f*PI/180.0f, glm::vec3(0.0f,1.0f,0.0f));
            drawObject(wonszCialoRogVao,shaderProgram,P,V,Mb[i], wonszCialoRog.get_vericles_count(), wonszCialo.get_texture());
        } else if(wonsz.Get_rotate_info(i)==2) {
            drawObject(wonszCialoRogVao,shaderProgram,P,V,Mb[i], wonszCialoRog.get_vericles_count(), wonszCialo.get_texture());
        }



    }

    glm::mat4 MJ = jabzo.designate_apple_position();
    MJ = glm::rotate(MJ, 270.0f*PI/180.0f, glm::vec3(1.0f,0.0f,0.0f));

    drawObject(jabzoVao,shaderProgram,P,V,MJ, jabzo.get_vericles_count(), jabzo.get_texture());

    ramka(P,V);
	//Przerzuæ tylny bufor na przedni
	glfwSwapBuffers(window);

}



void ramka(glm::mat4 P, glm::mat4 V)
{
    float H = MAX_H+2;
    float W = MAX_W+2;


    glm::mat4 Left_Wall_M[(MAX_H+2)*3+1];
    glm::mat4 Right_Wall_M[(MAX_H+2)*3+1];
    glm::mat4 Up_Wall_M[(MAX_W+2)*3+1];
    glm::mat4 Down_Wall_M[(MAX_W+2)*3+1];



    for(int i=0;i<=W*3-3;i+=2)
    {
        Up_Wall_M[i] = glm::mat4(1.0f);
        float x = i - W-1;
        Up_Wall_M[i] = glm::scale(Up_Wall_M[i], glm::vec3(SCALE,SCALE,1));
        Up_Wall_M[i] = glm::translate(Up_Wall_M[i], glm::vec3(x-(0.55*i/2),H+1,0.0f));
        Up_Wall_M[i] = glm::rotate(Up_Wall_M[i], -90.0f*PI/180.0f, glm::vec3(1.0f,0.0f,0.0f));
        drawObject(plotVao,shaderProgram,P,V,Up_Wall_M[i], plot.get_vericles_count(), plot.get_texture());

        Down_Wall_M[i] = glm::mat4(1.0f);
        Down_Wall_M[i] = glm::scale(Down_Wall_M[i], glm::vec3(SCALE,SCALE,1));
        Down_Wall_M[i] = glm::translate(Down_Wall_M[i], glm::vec3(x-(0.55*i/2),-H-2,0.0f));
        Down_Wall_M[i] = glm::rotate(Down_Wall_M[i], 270.0f*PI/180.0f, glm::vec3(1.0f,0.0f,0.0f));

        drawObject(plotVao,shaderProgram,P,V,Down_Wall_M[i], plot.get_vericles_count(), plot.get_texture());
    }
    for(int i=0;i<=H*3-3;i+=2)
    {
        Left_Wall_M[i] = glm::mat4(1.0f);
        float x = i - H -1;
         Left_Wall_M[i] = glm::scale(Left_Wall_M[i], glm::vec3(SCALE,SCALE,1));
        Left_Wall_M[i] = glm::translate(Left_Wall_M[i], glm::vec3(W + 2,x-(0.55*i/2),0.0f));
        Left_Wall_M[i] = glm::rotate(Left_Wall_M[i], -90.0f*PI/180.0f, glm::vec3(0.0f,0.0f,1.0f));
        Left_Wall_M[i] = glm::rotate(Left_Wall_M[i], -100.0f*PI/180.0f, glm::vec3(1.0f,0.0f,0.0f));
        drawObject(plotVao,shaderProgram,P,V,Left_Wall_M[i], plot.get_vericles_count(), plot.get_texture());

        Right_Wall_M[i] = glm::mat4(1.0f);
        Right_Wall_M[i] = glm::scale(Right_Wall_M[i], glm::vec3(SCALE,SCALE,1));
        Right_Wall_M[i] = glm::translate(Right_Wall_M[i], glm::vec3(-W - 2,x-(0.55*i/2),0.0f));
        Right_Wall_M[i] = glm::rotate(Right_Wall_M[i], -90.0f*PI/180.0f, glm::vec3(0.0f,0.0f,1.0f));
        Right_Wall_M[i] = glm::rotate(Right_Wall_M[i], -80.0f*PI/180.0f, glm::vec3(1.0f,0.0f,0.0f));
        drawObject(plotVao,shaderProgram,P,V,Right_Wall_M[i], plot.get_vericles_count(), plot.get_texture());
    }
}

int main(void)
{
	GLFWwindow* window; //WskaŸnik na obiekt reprezentuj¹cy okno
	GLFWmonitor* monitor;

	glfwSetErrorCallback(error_callback);//Zarejestruj procedurê obs³ugi b³êdów

	if (!glfwInit()) { //Zainicjuj bibliotekê GLFW
		fprintf(stderr, "Nie mo¿na zainicjowaæ GLFW.\n");
		exit(EXIT_FAILURE);
	}
    monitor = glfwGetPrimaryMonitor();
	window = glfwCreateWindow(1920, 1080, "Snake 3D", NULL, NULL);  //Utwórz okno 500x500 o tytule "OpenGL" i kontekst OpenGL.

	if (!window) //Je¿eli okna nie uda³o siê utworzyæ, to zamknij program
	{
		fprintf(stderr, "Nie mo¿na utworzyæ okna.\n");
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window); //Od tego momentu kontekst okna staje siê aktywny i polecenia OpenGL bêd¹ dotyczyæ w³aœnie jego.
	glfwSwapInterval(1); //Czekaj na 1 powrót plamki przed pokazaniem ukrytego bufora

	if (glewInit() != GLEW_OK) { //Zainicjuj bibliotekê GLEW
		fprintf(stderr, "Nie mo¿na zainicjowaæ GLEW.\n");
		exit(EXIT_FAILURE);
	}

	initOpenGLProgram(window); //Operacje inicjuj¹ce

    ruch = 5;
	glfwSetTime(0); //Wyzeruj licznik czasu

	//G³ówna pêtla
	while (!glfwWindowShouldClose(window)) //Tak d³ugo jak okno nie powinno zostaæ zamkniête
	{
    std::vector<float> forbidden_x;
    std::vector<float> forbidden_y;
    forbidden_x.push_back(wonsz.Get_head_pos_x());
    forbidden_y.push_back(wonsz.Get_head_pos_y());
    for(int i=0;i<wonsz.length-1;i++)
    {
        forbidden_x.push_back(wonsz.Get_body_pos_x(i));
        forbidden_y.push_back(wonsz.Get_body_pos_y(i));
    }


        if(pause == 0)
        {
            ruch = (ruch+1)%4;

            if(ruch == 0)
            {
                wonsz.movement();
                if(wonsz.has_colision())
                {
                PlaySound("Game_Over.wav", NULL, SND_ASYNC);
                pause = 1;
                }
            }

        }


    if(wonsz.Get_head_pos_x() == jabzo.get_x() && wonsz.Get_head_pos_y() == jabzo.get_y())
    {
        if(music!=true)PlaySound("coin.wav", NULL, SND_ASYNC);
        jabzo.is_eaten();
        wonsz.eat();
    }

		drawScene(window); //Wykonaj procedurê rysuj¹c¹
		glfwPollEvents(); //Wykonaj procedury callback w zaleznoœci od zdarzeñ jakie zasz³y.
	}

	freeOpenGLProgram();

	glfwDestroyWindow(window); //Usuñ kontekst OpenGL i okno
	glfwTerminate(); //Zwolnij zasoby zajête przez GLFW
	exit(EXIT_SUCCESS);
}

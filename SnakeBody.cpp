#include "SnakeBody.h"

namespace Models {
    SnakeBody::SnakeBody()
    {
        ObjectLoader::loadOBJ("models/wonsz/cialo_wonsz.obj", vertices, uvs, normals, colors);
    }

    void SnakeBody::loadTexture() {
        texture = ObjectLoader::loadTexture("models/wonsz/cialo_wonsz.png");
    };

    std::vector< glm::vec4 > SnakeBody::get_vertices() {
        return vertices;
    };

    std::vector< glm::vec2 > SnakeBody::get_uvs() {
        return uvs;
    };

    std::vector< glm::vec4 > SnakeBody::get_normals() {
        return normals;
    };

    std::vector< glm::vec4 > SnakeBody::get_colors() {
        return colors;
    };

    int SnakeBody::get_vericles_count() {
        return vertices.size();
    }

    GLuint SnakeBody::get_texture() {
        return texture;
    }

    SnakeBody::~SnakeBody()
    {
        //dtor
    }
}

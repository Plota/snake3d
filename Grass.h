#ifndef GRASS_H
#define GRASS_H

#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "ObjectLoader.h"
#include "constants.h"

namespace Models {
    class Grass
    {
        public:
            Grass();
            virtual ~Grass();
            void loadTexture();
            std::vector< glm::vec4 > get_vertices();
            std::vector< glm::vec2 > get_uvs();
            std::vector< glm::vec4 > get_normals();
            std::vector< glm::vec4 > get_colors();
            glm::mat4 get_M_matrix();
            GLuint get_texture();
            int get_vericles_count();
        protected:

        private:
            std::vector< glm::vec4 > vertices;
            std::vector< glm::vec2 > uvs;
            std::vector< glm::vec4 > normals;
            std::vector< glm::vec4 > colors;
            GLuint texture;
    };
}
#endif // GRASS_H

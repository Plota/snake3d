#define MAX_H 15
#define MAX_W 30
#define SCALE 0.3

#include "Apple.h"

Apple::Apple()
{
    srand( time( NULL ) );
    int x = ( std::rand() % (MAX_W-2)) + ((-MAX_W)/2+2);
    int y = ( std::rand() % (MAX_H-2)) + ((-MAX_H-1)/2+1);
    while(x == 0 || y == 0)
    {
        x = ( std::rand() % (MAX_W-2)) + ((-MAX_W)/2+2);
        y = ( std::rand() % (MAX_H-2)) + ((-MAX_H-1)/2+1);
    }
    pos_x = (float)x*2;
    pos_y = (float)y*2;
    apple_M = glm::mat4(1.0f);
    apple_M = glm::scale(apple_M, glm::vec3(SCALE,SCALE,SCALE));

    bool res = ObjectLoader::loadOBJ("models/japko/japko.obj", vertices, uvs, normals, colors);
}

float Apple::get_x()
{
    return pos_x;
}

float Apple::get_y()
{
    return pos_y;
}

void Apple::loadTexture() {
    texture = ObjectLoader::loadTexture("models/japko/japko.png");
};

std::vector< glm::vec4 > Apple::get_vertices() {
    return vertices;
};

std::vector< glm::vec2 > Apple::get_uvs() {
    return uvs;
};

std::vector< glm::vec4 > Apple::get_normals() {
    return normals;
};

std::vector< glm::vec4 > Apple::get_colors() {
    return colors;
};

GLuint Apple::get_texture() {
    return texture;
}

int Apple::get_vericles_count() {
    return vertices.size();
}

glm::mat4 Apple::designate_apple_position()
{
    return glm::translate(apple_M, glm::vec3(pos_x,pos_y,0.0f));
}

void Apple::is_eaten()
{
    int x = ( std::rand() % (MAX_W-2)) + ((-MAX_W)/2+2);
    int y = ( std::rand() % (MAX_H-2)) + ((-MAX_H-1)/2+1);
    pos_x = (float)x*2;
    pos_y = (float)y*2;
}

void Apple::drawSolid() {
    glEnable(GL_NORMALIZE);

    glEnableClientState(GL_VERTEX_ARRAY);
    //glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glVertexPointer(4,GL_FLOAT,0, vertices.data());
    //glColorPointer(4,GL_FLOAT,0,colors.data());
    glNormalPointer(GL_FLOAT,sizeof(float)*4, normals.data());
    glTexCoordPointer(2,GL_FLOAT,0,uvs.data());

    glDrawArrays(GL_TRIANGLES,0,get_vericles_count());

    glDisableClientState(GL_VERTEX_ARRAY);
    //glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

Apple::~Apple()
{
    //dtor
}

#include "SnakeBodyCorner.h"

namespace Models {
    SnakeBodyCorner::SnakeBodyCorner()
    {
        bool res = ObjectLoader::loadOBJ("models/wonsz/cialo_rog_wonsz.obj", vertices, uvs, normals, colors);
    }

    std::vector< glm::vec4 > SnakeBodyCorner::get_vertices() {
        return vertices;
    };

    std::vector< glm::vec2 > SnakeBodyCorner::get_uvs() {
        return uvs;
    };

    std::vector< glm::vec4 > SnakeBodyCorner::get_normals() {
        return normals;
    };

    std::vector< glm::vec4 > SnakeBodyCorner::get_colors() {
        return colors;
    };

    int SnakeBodyCorner::get_vericles_count() {
        return vertices.size();
    }

    SnakeBodyCorner::~SnakeBodyCorner()
    {
        //dtor
    }
}
